import os
import sys
import meowcode


def check_flag():
    flag = os.environ['FLAG']
    for letter in flag:
        if letter not in meowcode.meowletters:
            print(f"Flag has unsupported letter: {letter}")
            sys.exit()


def encode(input_file, output_file):
    with open(input_file, 'r') as file:
        text = file.read() + " " + os.environ['FLAG']
    encoded = ''

    for i in text:
        encoded += meowcode.meowdict[i.lower()]

    with open(output_file, "w") as f:
        f.write(encoded)


def decode(input_file, output_file):
    with open(input_file, 'r') as file:
        text = file.read().replace(' ', '')
    decoded = ''

    for i in range(0, len(text), 4):
        decoded += meowcode.meowdict_inv[text[i:i+4]]

    with open(output_file, "w") as f:
        f.write(decoded)


if __name__ == '__main__':
    print(os.getcwd())
    if sys.argv[1] == 'encode':
        check_flag()
        encode(sys.argv[2], sys.argv[3])
    elif sys.argv[1] == 'decode':
        decode(sys.argv[2], sys.argv[3])
    else:
        print("""Usage:
                    python3 task.py [encode/decode] <input file> <output file>""")
